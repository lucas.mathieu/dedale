package eu.su.mas.dedaleEtu.mas.knowledge;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;

import java.util.stream.Collectors;
import java.util.Set;
import java.util.HashSet;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.EdgeRejectedException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;

/**
 * This simple topology representation only deals with the graph, not its content.</br>
 * The knowledge representation is not well written (at all), it is just given as a minimal example.</br>
 * The viewer methods are not independent of the data structure, and the dijkstra is recomputed every-time.
 *
 * @author hc
 */
public class MapRepresentation
implements Serializable {

	public enum MapAttribute {
		agent,
		open,
		close
	}

	private static final long serialVersionUID = -1333959882640838272L;

	private Graph g; //data structure
	private transient Viewer viewer; //ref to the display
	private int nbEdges; //used to generate the edges ids
	private SerializableGraph sg;

	// Exploration purpose
	private List<Node> openedNodes;
	private Set<Node>  closedNodes;


	// Parameters for graph rendering
	private final String defaultNodeStyle = "node {" + "fill-color: black;" + " size-mode:fit;text-alignment:under; text-size:14;text-color:white;text-background-mode:rounded-box;text-background-color:black;}";
	private final String nodeStyle_open = "node.agent {" + "fill-color: forestgreen;" + "}";
	private final String nodeStyle_agent = "node.open {" + "fill-color: blue;" + "}";
	private final String nodeStyle_gold = "node.Gold {" + "fill-color: yellow;" + "}";
	private final String nodeStyle = this.defaultNodeStyle + this.nodeStyle_agent + this.nodeStyle_open + this.nodeStyle_gold;

	public MapRepresentation() {
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");

		this.g = new SingleGraph("My world vision");
		this.g.setAttribute("ui.stylesheet", this.nodeStyle);
		this.nbEdges = 0;

		this.openedNodes = new ArrayList<>();
		this.closedNodes = new HashSet<>();
	}

	/**
	 * Associate to a node an attribute in order to identify them by type.
	 *
	 * @param id
	 * @param mapAttribute
	 */
	public void addNode(final String id, final MapAttribute mapAttribute) {
		this.addNode(id);
		Node node = this.g.getNode(id);
		switch (mapAttribute) {
			case open:
				node.addAttribute("explore.state", mapAttribute.toString());
				node.addAttribute("ui.class", mapAttribute.toString());
			break;

			case close:
				node.addAttribute("explore.state", mapAttribute.toString());
			break;

			case agent:
				node.addAttribute("ui.class", mapAttribute.toString());
			break;

			default:
				;;
			break;
		}
		if (mapAttribute == MapAttribute.open && !this.openedNodes.contains(node)) {
			this.openedNodes.add(node);
		}
	}


	/**
	 * Associate to a node a String data.
	 *
	 * @param id
	 * @param observations
	 */
	public void addNode(final String id, final List<Couple<Observation, Integer>> observations) {
		this.addNode(id);
		Node node = this.g.getNode(id);

		for (final Couple<Observation, Integer> obs : observations) {
			final Observation type = obs.getLeft();
			final Integer value = obs.getRight();
			node.addAttribute(type.toString(), value);
			if (type == Observation.GOLD) {
				node.addAttribute("ui.class", type.toString());
				// node.addAttribute("ui.style", "fill-color: yellow;");
				node.addAttribute("ui.label", id+",G:"+value);
			}
		}
	}

	/**
	 * Add the node id if not already existing
	 *
	 * @param id
	 */
	public void addNode(final String id) {
		Node n = this.g.getNode(id);
		if (n == null) {
			n = this.g.addNode(id);
			n.addAttribute("ui.label", id);
		}
	}

	public void markAgent(final String id) {
		Node node = this.g.getNode(id);
		node.addAttribute("agent", true);
		this.updateNodeStyle(node);
	}

	public void unmarkAgent(final String id) {
		Node node = this.g.getNode(id);
		node.removeAttribute("agent");
		this.updateNodeStyle(node);
	}


	/**
	 * Marks a node as opened.
	 * @param id Identifier of the node.
	 */
	public void markOpened(final String id) {
		Node node = this.g.getNode(id);
		if (!this.isOpened(id)) {
			node.addAttribute("explore.state", MapAttribute.open.toString());
			this.openedNodes.add(node);
			this.closedNodes.remove(node);
			this.updateNodeStyle(node);
		}
	}

	/**
	 * Marks a node as closed.
	 * @param id Identifier of the node.
	 */
	public void markClosed(final String id) {
		Node node = this.g.getNode(id);
		if (!this.isClosed(id)) {
			node.addAttribute("explore.state", MapAttribute.close.toString());
			this.openedNodes.remove(node);
			this.closedNodes.add(node);
		}
	}

	/**
	 * Returns true if the node is marked opened.
	 * @param  id Identifier of the node.
	 * @return    true is the node is marked opened. false otherwise.
	 */
	public boolean isOpened(final String id) {
		return MapAttribute.open.toString().equals(this.g.getNode(id).getAttribute("explore.state"));
	}

	/**
	 * Returns true if the node is not marked opened.
	 * @param  id Identifier of the node.
	 * @return    true is the node is not marked closed. false otherwise.
	 */
	public boolean isClosed(final String id) {
		return MapAttribute.close.toString().equals(this.g.getNode(id).getAttribute("explore.state"));
	}

	/**
	 * Returns a list of opened nodes identifiers.
	 * @return The list of opened nodes identifiers.
	 */
	public List<String> getOpenedNodes() {
		return this.openedNodes.stream().map(node -> node.getId()).collect(Collectors.toList());
	}

	/**
	* Returns a list of closed nodes identifiers.
	* @return The list of closed nodes identifiers.
	*/
	public Set<String> getClosedNodes() {
		return this.openedNodes.stream().map(node -> node.getId()).collect(Collectors.toSet());
	}


	/**
	 * Add the edge if not already existing.
	 *
	 * @param idNode1
	 * @param idNode2
	 */
	public void addEdge(final String idNode1, final String idNode2) {
		try {
			this.nbEdges++;
			this.g.addEdge(Integer.toString(this.nbEdges), idNode1, idNode2);
		} catch (final EdgeRejectedException e) {
			//Do not add an already existing one
			this.nbEdges--;
		}
	}

	/**
	 * Compute the shortest Path from idFrom to IdTo. The computation is currently not very efficient
	 *
	 * @param idFrom id of the origin node
	 * @param idTo id of the destination node
	 * @return the list of nodes to follow
	 */
	public List<String> getShortestPath(final String idFrom, final String idTo) {
		final List<String> shortestPath = new ArrayList<>();

		final Dijkstra dijkstra = new Dijkstra();//number of edge
		dijkstra.init(this.g);
		dijkstra.setSource(this.g.getNode(idFrom));
		dijkstra.compute();//compute the distance to all nodes from idFrom
		final List<Node> path = dijkstra.getPath(this.g.getNode(idTo)).getNodePath(); //the shortest path from idFrom to idTo
		final Iterator<Node> iter = path.iterator();
		while (iter.hasNext()) {
			shortestPath.add(iter.next().getId());
		}
		dijkstra.clear();
		shortestPath.remove(0);//remove the current position
		return shortestPath;
	}

	/**
	 * Returns the path to the closest node in the list.
	 *
	 * @param from  The node source
	 * @param nodes The list of allowed nodes
	 * @return The path to the closest node, or null if there is not any
	 */
	public List<String> getPathToClosestNode(final String from, final List<String> nodes) {
		if (nodes.size() <= 0) {
			return null;
		}
		// Compute shortest paths
		final Dijkstra dijkstra  = new Dijkstra();
		dijkstra.init(this.g);
		dijkstra.setSource(this.g.getNode(from));
		dijkstra.compute();

		// Find the closest node
		String closest = nodes.get(0);
		double length = dijkstra.getPathLength(this.g.getNode(closest));;
		for (final String node : nodes) {
			final double l = dijkstra.getPathLength(this.g.getNode(node));
			if (l < length) {
				closest = node;
				length = l;
			}
		}

		// Construct path
		final List<String> result = new ArrayList<>();
		// Nodes are iterated from dest to source
		for (final Node node : dijkstra.getPathNodes(this.g.getNode(closest))) {
			result.add(0, node.getId());
		}
		result.remove(0);
		dijkstra.clear();

		return result;
	}


	public SerializableGraph getSerializableGraph() {
		return new SerializableGraph(this.g);
	}

	// NOTE: Does not cover empty graphs
	public boolean isComplete() {
		return this.closedNodes.size() == this.g.getNodeCount() && this.g.getNodeCount() > 0;
	}


	private void updateNodeStyle(Node node) {
		// Style priority
		String cl = null;
		if (node.getAttribute("agent") != null && node.getAttribute("agent").equals(true)) {
			cl = MapAttribute.agent.toString();
		}
		else if (node.getAttribute(Observation.GOLD.toString()) != null) {
			cl = Observation.GOLD.toString();
		}
		else if (this.isOpened(node.getId())) {
			cl = MapAttribute.open.toString();
		}
		else {
			;;
		}

		if (cl != null) {
			node.addAttribute("ui.class", cl);
		}
		else {
			node.removeAttribute("ui.class");
		}
	}

	private void updateGraphStyle() {
		for (Node node : this.g) {
			this.updateNodeStyle(node);
		}
	}


	// Takes all the attributes from source and replaces them into dest, except
	// when "ui.class" -> "open" : it does not modify the previous value.
	private void mergeNode(Node source, Node dest) {
		source.getAttributeKeyIterator().forEachRemaining(
			key -> {
				Object value = source.getAttribute(key);
				// Special case for explore.state : only updating state if it's closed
				if (key.equals("explore.state")) {
					if (MapAttribute.close.toString().equals(value)) {
						this.markClosed(dest.getId());
					}
				}
				else if (key.equals("agent")) {
					;;
				}
				else {
					dest.addAttribute(key, value);
				}
			}
		);
	}

	// TODO : Improve how to merge (time-based information ?)
	public void mergeGraph(final SerializableGraph graph) {
		Graph gr = graph.toGraph();

		// Merge nodes
		for (final Node node : gr) {
			// Create node if not exists
			String id = node.getId();
			Node n = this.g.getNode(id);
			if (n == null) {
				n = this.g.addNode(id);
			}

			// Merge nodes
			this.mergeNode(node, n);
		}

		// Merge edges
		for (final Couple<String, String> edge : graph.getEdges()) {
			addEdge(edge.getLeft(), edge.getRight());
		}

		this.updateGraphStyle();
	}

	public void prepareMigration() {
		this.sg = new SerializableGraph(this.g);
		if (this.viewer != null) {
			this.viewer.close();
			this.viewer = null;
		}
		this.g = null;
	}

	public void afterMigration() {
		this.g = this.sg.toGraph();
		this.g.setAttribute("ui.stylesheet", this.nodeStyle);
		this.sg = null;
	}

	public void showGui(String title) {
		if (this.viewer == null) {
			this.viewer = this.g.display();
			((JFrame) SwingUtilities.getWindowAncestor(this.viewer.getDefaultView())).setTitle(title);
		}
	}

	public Collection<Node> getNodes() {
		return this.g.getNodeSet();
	}
}
