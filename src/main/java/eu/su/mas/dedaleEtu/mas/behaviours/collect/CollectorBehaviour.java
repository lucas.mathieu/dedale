package eu.su.mas.dedaleEtu.mas.behaviours.collect;

import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.SimpleBehaviour;

public class CollectorBehaviour extends FSMBehaviour {
	private static final long serialVersionUID = 5553077394407070442L;

	public static final String STATE_MEET_TANK = "meet_tank";
	public static final String STATE_REQUEST_ORDER  = "request_order";
	public static final String STATE_GOTO_GOLD      = "goto_gold";
	public static final String STATE_COLLECT        = "collect";
	public static final String STATE_GOTO_TANK      = "goto_tank";
	public static final String STATE_SUMMARY        = "summary";
	public static final String STATE_BLOCK          = "block";
	public static final String STATE_FINAL          = "final";

	public static final int EV_MET_TANK        = 1;
	public static final int EV_GOT_ORDER       = 2;
	public static final int EV_NO_MORE_ORDERS  = 3;
	public static final int EV_AT_GOLD         = 4;
	public static final int EV_COLLECTED       = 5;
	public static final int EV_RETURNED        = 6;
	public static final int EV_AVAILABLE       = 7;
	public static final int EV_BLOCKED         = 8;

	public CollectorBehaviour(final CollectorAgent agent) {
		super(agent);

		final MoveTowardsTankBehaviour mttb = new MoveTowardsTankBehaviour(agent);
		final RequestOrderBehaviour rob = new RequestOrderBehaviour(agent);
		final MoveTowardsGoldBehaviour mtgb = new MoveTowardsGoldBehaviour(agent);
		final CollectGoldBehaviour cgb = new CollectGoldBehaviour(agent);
		final ReturnToTankBehaviour rttb = new ReturnToTankBehaviour(agent);
		final MakeSummaryBehaviour msb = new MakeSummaryBehaviour(agent);

		this.registerFirstState(mttb, STATE_MEET_TANK);
		this.registerState(rob, STATE_REQUEST_ORDER);
		this.registerState(mtgb, STATE_GOTO_GOLD);
		this.registerState(cgb, STATE_COLLECT);
		this.registerState(rttb, STATE_GOTO_TANK);
		this.registerState(msb, STATE_SUMMARY);
		this.registerLastState(new SimpleBehaviour(){
			private static final long serialVersionUID = -5824570634792899333L;

			@Override
			public boolean done() {
				return true;
			}

			@Override
			public void action() {
				agent.println("I've got no more orders, bye bye!");
			}
		}, STATE_FINAL);

		// TODO Handle interblocking event
		this.registerTransition(STATE_MEET_TANK, STATE_REQUEST_ORDER, EV_MET_TANK);
		this.registerTransition(STATE_REQUEST_ORDER, STATE_GOTO_GOLD, EV_GOT_ORDER);
		this.registerTransition(STATE_REQUEST_ORDER, STATE_FINAL, EV_NO_MORE_ORDERS);
		this.registerTransition(STATE_GOTO_GOLD, STATE_COLLECT, EV_AT_GOLD);
		this.registerTransition(STATE_COLLECT, STATE_GOTO_TANK, EV_COLLECTED);
		this.registerTransition(STATE_GOTO_TANK, STATE_SUMMARY, EV_RETURNED);
		this.registerTransition(STATE_SUMMARY, STATE_REQUEST_ORDER, EV_AVAILABLE);
	}
}
