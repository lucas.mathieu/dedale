package eu.su.mas.dedaleEtu.mas.behaviours.dispatch;

import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.share.ShareMapBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.FSMBehaviour;

public class TankerBehaviour extends FSMBehaviour{
	private static final long serialVersionUID = -2068801241761679725L;

	public static final String STATE_SHARE = "share";
	public static final String STATE_DISPATCH = "dispatch";

	public static final int TRANSITION_DISPATCH_SHARE	= 1;
	public static final int TRANSITION_SHARE_DISPATCH 	= 2;


	public TankerBehaviour(final AbstractAgent agent, final MapRepresentation map) {
		super(agent);
		final ShareMapBehaviour shareBehaviour = new ShareMapBehaviour(agent, map);
		final DispatcherBehaviour dispatcherBehaviour = new DispatcherBehaviour(agent);

		this.registerFirstState(shareBehaviour, STATE_SHARE);
		this.registerState(dispatcherBehaviour, STATE_DISPATCH);

		this.registerTransition(STATE_SHARE, STATE_DISPATCH, TRANSITION_SHARE_DISPATCH);
		this.registerTransition(STATE_DISPATCH, STATE_SHARE, TRANSITION_DISPATCH_SHARE);
	}

	public String getLastState() {
		return this.getName(this.getPrevious());
	}
}
