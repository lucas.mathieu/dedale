package eu.su.mas.dedaleEtu.mas.behaviours.explore;

import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.share.ShareMapBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.unlock.LockPickBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.FSMBehaviour;


public class ExplorationBehaviour extends FSMBehaviour {
	public static final String STATE_EXPLORE = "explore";
	public static final String STATE_SHARE = "share";
	public static final String STATE_BLOCK = "block";
	public static final String STATE_FINAL = "final";

	public static final int TRANSITION_EXPLORE_FINAL 	= 0;
	public static final int TRANSITION_EXPLORE_SHARE 	= 1;
	public static final int TRANSITION_EXPLORE_BLOCK 	= 2;
	public static final int TRANSITION_SHARE_EXPLORE 	= 3;
	public static final int TRANSITION_SHARE_BLOCK 		= 4;
	public static final int TRANSITION_BLOCK_EXPLORE 	= 5;
	public static final int TRANSITION_BLOCK_SHARE 		= 6;
	private static final int TRANSITION_BLOCK_FINAL 	= 7;
	private static final int TRANSITION_FINAL_BLOCK		= 8;

	public ExplorationBehaviour (final AbstractAgent agent, final MapRepresentation map) {
		super(agent);
		final ExploMultiBehaviour exploBehaviour = new ExploMultiBehaviour(agent, map);
		final ShareMapBehaviour shareBehaviour = new ShareMapBehaviour(agent, map);
		final InterBlockingBehaviour blockBehaviour = new InterBlockingBehaviour(agent);
		final LockPickBehaviour lockPickBehaviour = new LockPickBehaviour(agent, map);

		this.registerFirstState(exploBehaviour, STATE_EXPLORE);
		this.registerState(shareBehaviour, STATE_SHARE);
		this.registerState(blockBehaviour, STATE_BLOCK);
		this.registerState(lockPickBehaviour, STATE_FINAL);

		this.registerTransition(STATE_EXPLORE, STATE_FINAL, TRANSITION_EXPLORE_FINAL);
		this.registerTransition(STATE_EXPLORE, STATE_SHARE, TRANSITION_EXPLORE_SHARE);
		this.registerTransition(STATE_EXPLORE, STATE_BLOCK, TRANSITION_EXPLORE_BLOCK);
		this.registerTransition(STATE_SHARE, STATE_EXPLORE, TRANSITION_SHARE_EXPLORE);
		this.registerTransition(STATE_SHARE, STATE_BLOCK, TRANSITION_SHARE_BLOCK);
		this.registerTransition(STATE_BLOCK, STATE_EXPLORE, TRANSITION_BLOCK_EXPLORE);
		this.registerTransition(STATE_BLOCK, STATE_SHARE, TRANSITION_BLOCK_SHARE);
		this.registerTransition(STATE_BLOCK, STATE_FINAL, TRANSITION_BLOCK_FINAL);
		this.registerTransition(STATE_FINAL, STATE_BLOCK, TRANSITION_FINAL_BLOCK);
	}

	public String getLastState() {
		return this.getName(this.getPrevious());
	}
}
