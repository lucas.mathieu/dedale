package eu.su.mas.dedaleEtu.mas.agents;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.explore.ExplorationBehaviour;
import jade.core.behaviours.Behaviour;

public class ExploreAgent extends AbstractAgent {
	private static final long serialVersionUID = 6125161910289436938L;

	@Override
	protected void setup() {
		super.setup();

		final Object[] args = this.getArguments();
		// Handle arguments

		final List<Behaviour> lb = new ArrayList<>();
		final ExplorationBehaviour exploFSM = new ExplorationBehaviour(this, this.map);
		lb.add(exploFSM);

		addBehaviour(new startMyBehaviours(this, lb));
	}

	@Override
	protected void beforeMove() {
		super.beforeMove();
	}

	@Override
	protected void afterMove() {
		super.afterMove();
		this.map.showGui(this.getLocalName());
	}
}
