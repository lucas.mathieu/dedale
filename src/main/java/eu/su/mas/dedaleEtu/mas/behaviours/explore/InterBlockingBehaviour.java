package eu.su.mas.dedaleEtu.mas.behaviours.explore;

import jade.core.behaviours.FSMBehaviour;
import java.util.List;
import java.util.Random;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.share.CommunicationBehaviour;
import jade.core.behaviours.CompositeBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class InterBlockingBehaviour
extends CommunicationBehaviour {
	private static final long serialVersionUID = 1487972057054972388L;

	private boolean finished;
	private int transition;

	private static final int COMMUNICATION_TIMEOUT = 20;

	private final MessageTemplate blockedTemplate = MessageTemplate.MatchContent("blocked");


	public InterBlockingBehaviour (final AbstractAgent agent) {
		super(agent);
		this.finished = false;
	}

	@Override
	public void action () {
		System.out.println(this.myAgent.getLocalName() + " stopped. Sending blocked.");
		this.sendBlocked();
		final ACLMessage msg = this.waitMessage(this.blockedTemplate, InterBlockingBehaviour.COMMUNICATION_TIMEOUT);
		final AbstractAgent me = ((AbstractAgent) this.myAgent);

		if (msg != null) {
			System.out.println(this.myAgent.getLocalName() + " is in conflict with " + msg.getSender().getLocalName());
			final boolean priority = msg.getSender().compareTo(this.myAgent.getAID()) == 1;
			if (!priority) {
				// Try to move to a random node.
				System.out.println(me.getLocalName() + ": moving away");
				final List<Couple<String, List<Couple<Observation, Integer>>>> observe = me.observe();
				final int nextNode = new Random().nextInt(observe.size());
				me.moveTo(observe.get(nextNode).getLeft());

				clearOldMessages(msg.getSender());

				me.doWait(500);
			} else {
				me.doWait(100);
			}
		} else {
			System.out.println(this.myAgent.getLocalName() + " timed out. Try to move again ?");
			// Timeout: try to move again in case the way is cleared
			quit();
		}
	}

	private void sendBlocked() {
		this.sendMessage(getAllAgents(), "blocked", ACLMessage.REQUEST);
	}

	public void quit() {
		final CompositeBehaviour cb = this.getParent();
		if (cb instanceof ExplorationBehaviour) {
			final ExplorationBehaviour eb = (ExplorationBehaviour) cb;
			final String st = eb.getLastState();
			if (st.equals(ExplorationBehaviour.STATE_EXPLORE)) {
				this.transition = ExplorationBehaviour.TRANSITION_BLOCK_EXPLORE;
			} else if (st.equals(ExplorationBehaviour.STATE_SHARE)) {
				this.transition = ExplorationBehaviour.TRANSITION_BLOCK_SHARE;
			} else {
				this.transition = -1; // FIXME
			}
		} else {
			this.transition = -1; // FIXME
		}
		this.finished = true;
	}

	@Override
	public int onEnd() {
		return this.transition;
	}

	@Override
	public boolean done () {
		if (this.finished) {
			this.finished = false;
			return true;
		}
		return false;
	}

}
