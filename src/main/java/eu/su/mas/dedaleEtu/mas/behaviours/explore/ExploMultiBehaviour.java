package eu.su.mas.dedaleEtu.mas.behaviours.explore;

import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.princ.ConfigurationFile;

import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.SerializableGraph;
import jade.core.behaviours.SimpleBehaviour;

public class ExploMultiBehaviour
extends SimpleBehaviour {
	private static final long serialVersionUID = 2516237326966762071L;

	private boolean finished;
	private final MapRepresentation map;

	private int stepsFromLastPing;

	private int transition;

	public ExploMultiBehaviour(final AbstractAgent agent, final MapRepresentation map) {
		super(agent);
		this.map = map;
		this.stepsFromLastPing = 0;
		this.transition = 0;
	}

	@Override
	public void action() {
		final AbstractAgent me = (AbstractAgent) this.myAgent;
		final String pos = me.getCurrentPosition();
		// Not in the environment
		if (pos == null) {
			return;
		}

		// Agent is in environment

		// Timing between each step for visualization purposes
		try {
			me.doWait(500);
		} catch (final Exception e) {
			System.err.println("Agent " + me.getLocalName() + " failed to wait");
			e.printStackTrace();
		}

		// Data acquisition
		final List<Couple<String, List<Couple<Observation, Integer>>>> observations;
		observations = me.observe();

		// Explore

		// Current position is added
		this.map.addNode(pos);
		this.map.markClosed(pos);

		// Update map information with the observations
		for (final Couple<String, List<Couple<Observation, Integer>>> obs : observations) {
			final String node = obs.getLeft();
			final List<Couple<Observation, Integer>> lst = obs.getRight();

			// Updating observations
			this.map.addNode(node, lst);

			// The node we see is already closed. No need to do anything. -> cycle
			if (this.map.isClosed(node)) {
				;;
			}
			// The node is opened but not explored -> cycle
			else if (this.map.isOpened(node)) {
				// Add the edge discovered
				this.map.addEdge(pos, node);
			}
			// The node was undiscovered
			else {
				// Add node in map, and the edge corresponding
				this.map.addNode(node);
				this.map.addEdge(pos, node);
				// Mark that node open for explore
				this.map.markOpened(node);
			}
		}

		// Exploration done ?
		if (this.isExplorationDone()) {
			this.transition = ExplorationBehaviour.TRANSITION_EXPLORE_FINAL;
			this.finished = true;
			System.out.println("Agent " + me.getLocalName() + " finished exploration");
		}
		// Interrupt current exploration for sharing
		else if (this.wantToShare()) {
			this.transition = ExplorationBehaviour.TRANSITION_EXPLORE_SHARE;
			this.finished = true;
			this.stepsFromLastPing = 0;
		}
		else {
			// Choice of next destination
			final List<String> path = this.map.getPathToClosestNode(pos, this.map.getOpenedNodes());

			// Move
			final String dest = path.get(0);
			final boolean moved = me.moveTo(dest);
			// Failure moving
			if (!moved) {
				this.transition = ExplorationBehaviour.TRANSITION_EXPLORE_BLOCK;
				this.finished = true;
			}
			// Success moving
			else {
				this.stepsFromLastPing++;
				this.map.unmarkAgent(pos);
				this.map.markAgent(dest);
			}
		}
	}

	private boolean wantToShare() {
		return (this.stepsFromLastPing >= ConfigurationFile.DEFAULT_COMMUNICATION_REACH);
	}

	private boolean isExplorationDone() {
		return this.map.getOpenedNodes().isEmpty();
	}


	@Override
	public int onEnd() {
		return this.transition;
	}


	@Override
	public boolean done() {
		if (this.finished) {
			this.finished = false;
			return true;
		}
		return false;
	}

}
