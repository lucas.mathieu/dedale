package eu.su.mas.dedaleEtu.mas.behaviours.dispatch;

import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.share.CommunicationBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class DispatcherBehaviour
extends CommunicationBehaviour {
	private static final long serialVersionUID = -5235862432651795153L;

	private static final long MSG_TIMEOUT = 300;

	private int transition;
	private boolean finished;

	public DispatcherBehaviour(final AbstractAgent agent) {
		super(agent);
	}

	@Override
	public void action() {
		if (!((AbstractAgent) this.myAgent).getMap().isComplete()) {
			this.quit();
		} else {
			// Handle messages.
			final ACLMessage message = waitMessage(MessageTemplate.MatchProtocol("collection"), MSG_TIMEOUT);
			if (message != null) {
				final String content = message.getContent();
				final String[] line = content.split(" ");
				final String keyword = line[0];
				switch (keyword) {
					case "transfer":
						// TODO: traitement
						// final int gold = Integer.parseInt(line[1]);
					case "new-order":
						// TODO
					case "collect-failure":
						// TODO
						// final int goldPosition = Integer.parseInt(line[1]);
					case "collect-success":
						// TODO
						// final int goldPosition = Integer.parseInt(line[1]);
					default:
						// Non matching protocol message
						// Ignore
				}
			}
		}
	}

	public void quit() {
		this.transition = TankerBehaviour.TRANSITION_DISPATCH_SHARE;
		this.finished = true;
	}

	@Override
	public int onEnd() {
		return this.transition;
	}

	@Override
	public boolean done() {
		if (this.finished) {
			this.finished = false;
			return true;
		}
		return false;
	}
}
