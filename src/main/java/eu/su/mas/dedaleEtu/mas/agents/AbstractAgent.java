package eu.su.mas.dedaleEtu.mas.agents;

import java.util.List;

import eu.su.mas.dedale.env.EntityCharacteristics;
import eu.su.mas.dedale.env.EntityType;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;

public abstract class AbstractAgent extends AbstractDedaleAgent {
	private static final long serialVersionUID = 326908126675763355L;

	protected MapRepresentation map;

	// TODO : remove (cf getAgents)
	private List<String> agents;

	public AbstractAgent() {
		super();
	}

	public AbstractAgent(final EntityType e, final EntityCharacteristics ec, final String environmentName) {
		super(e, ec, environmentName);
	}

	@Override
	protected void setup() {
		super.setup();

		final Object[] args = this.getArguments();
		this.agents = (List<String>) this.getArguments()[2];

		this.map = new MapRepresentation();
	}

	public void debug(final Object s) {
		System.out.println("["+this.getLocalName()+"] {Debug} "+s);
	}

	public void println(final Object s) {
		System.out.println("["+this.getLocalName()+"] "+s);
	}

	// TODO : ask DF / AMS
	public List<String> getAgents() {
		return this.agents;
	}

	public MapRepresentation getMap() {
		return this.map;
	}

	public void setMap(final MapRepresentation map) {
		this.map = map;
	}

	@Override
	protected void beforeMove() {
		super.beforeMove();
		if (this.map != null) {
			this.map.prepareMigration();
		}
	}

	@Override
	protected void afterMove() {
		super.afterMove();
		if (this.map != null) {
			this.map.afterMigration();
		}
	}
}
