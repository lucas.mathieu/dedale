package eu.su.mas.dedaleEtu.mas.agents;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.dispatch.TankerBehaviour;
import jade.core.behaviours.Behaviour;

public class TankerAgent
extends AbstractAgent {
	private static final long serialVersionUID = 3946795943759286285L;

	@Override
	protected void setup() {
		super.setup();
		final List<Behaviour> lb = new ArrayList<>();
		lb.add(new TankerBehaviour(this, this.map));
		addBehaviour(new startMyBehaviours(this, lb));
	}

	@Override
	protected void beforeMove() {
		super.beforeMove();
	}

	@Override
	protected void afterMove() {
		super.afterMove();
		if (this.map.isComplete()) {
			this.map.showGui(this.getLocalName());
		}
	}
}
