package eu.su.mas.dedaleEtu.mas.behaviours.collect;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class RequestOrderBehaviour
extends SimpleBehaviour {
	private static final long serialVersionUID = 5675339036945031463L;

	private boolean finished;
	private int returnStatus;
	private final MessageTemplate collectionTemplate = MessageTemplate.MatchProtocol("collection");

	public RequestOrderBehaviour(final CollectorAgent agent) {
		super(agent);
		this.finished = false;
	}

	@Override
	public void action() {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		final ACLMessage msg = me.receive(this.collectionTemplate);
		if (msg != null) {
			final String[] args = msg.getContent().split(" ");
			switch (args[0]) {
				case "collect":
					me.setGoldPosition(args[1]);
					me.setJob(CollectorAgent.Job.COLLECT);
					this.finished = true;
					this.returnStatus = CollectorBehaviour.EV_GOT_ORDER;
					break;

				case "lockpick":
					me.setGoldPosition(args[1]);
					me.setJob(CollectorAgent.Job.LOCKPICK);
					this.finished = true;
					this.returnStatus = CollectorBehaviour.EV_GOT_ORDER;
					break;

				case "info":
					me.sendMessage(this.expertiseMessage());
					break;

				case "no-order":
					this.finished = true;
					this.returnStatus = CollectorBehaviour.EV_NO_MORE_ORDERS;
					break;

				default:
					me.println("Received off-protocol message: ");
					me.println("[" + msg.getContent() + "]");
					break;
			}
		} else {
			me.sendMessage(this.requestOrderMessage());
		}
	}

	private ACLMessage messageHeader(final int performative) {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		final ACLMessage msg = new ACLMessage(performative);
		msg.setProtocol("collection");
		msg.setSender(me.getAID());
		msg.addReceiver(new AID(me.getTankName(), true));

		return msg;
	}

	private ACLMessage expertiseMessage() {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		final ACLMessage msg = this.messageHeader(ACLMessage.INFORM);
		String content = "";
		content += "expertise";
		content += " " + me.getBackPackFreeSpace();
		content += " " + this.stringifyMyStats();
		msg.setContent(content);

		return msg;
	}

	private ACLMessage requestOrderMessage() {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		final ACLMessage msg = this.messageHeader(ACLMessage.REQUEST);

		String content = "";
		content += "new-order";
		msg.setContent(content);
		me.debug(content);
		return msg;
	}

	private String stringifyMyStats() {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		int strengh = -1;
		int lockpicking = -1;
		for (final Couple<Observation, Integer> c : me.getMyExpertise()) {
			final Observation type = c.getLeft();
			final Integer value = c.getRight();
			switch (type) {
				case STRENGH:
					strengh = value;
					break;

				case LOCKPICKING:
					lockpicking = value;
					break;

				default:
					;
					;
					break;
			}
		}
		return strengh + " " + lockpicking;
		// return me.getMyExpertise().stream()
		//   .sorted(
		//     (a, b) -> a.getLeft().toString().compareTo(b.getLeft().toString())
		//   )
		//   .map(c -> c.getLeft().toString()+":"+c.getRight())
		//   .reduce(
		//     "",
		//     (a, b) -> a+" "+b,
		//     (a, b) -> a+" "+b
		// );
	}

	@Override
	public boolean done() {
		return this.finished;
	}

	@Override
	public int onEnd() {
		this.finished = false;
		return this.returnStatus;
	}
}
