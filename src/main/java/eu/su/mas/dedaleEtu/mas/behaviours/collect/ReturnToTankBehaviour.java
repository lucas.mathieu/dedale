package eu.su.mas.dedaleEtu.mas.behaviours.collect;

import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.princ.ConfigurationFile;
import jade.core.behaviours.SimpleBehaviour;

public class ReturnToTankBehaviour
extends SimpleBehaviour {
	private static final long serialVersionUID = -8784347724194798835L;

	private boolean finished;
	private int returnStatus;
	private List<String> path;

	public ReturnToTankBehaviour(final AbstractAgent agent) {
		super(agent);
		this.finished = false;
	}

	@Override
	public void action() {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		final String currentPosition = me.getCurrentPosition();
		final String tankPosition = me.getTankPosition();
		final MapRepresentation map = me.getMap();
		final List<Couple<String, List<Couple<Observation, Integer>>>> observations;

		observations = me.observe();

		if (this.path == null) {
			this.path = map.getShortestPath(currentPosition, tankPosition);
		}
		// Arrived at tank position : this is weird
		if (this.path.size() == 0) {
			// TODO
			this.finished = true;
			this.returnStatus = CollectorBehaviour.EV_RETURNED;
		}
		// If the tank is reachable by communication
		else if (this.path.size() <= ConfigurationFile.DEFAULT_COMMUNICATION_REACH) {
			this.finished = true;
			this.returnStatus = CollectorBehaviour.EV_RETURNED;
		}
		// Move
		else {
			final String nextPos = this.path.get(0);
			boolean moved = me.moveTo(nextPos);
			int tries = 0;
			final int maxtries = 3;
			// Blocked
			while (!moved && tries < maxtries) {
				// Retry
				me.debug("I tried to move to " + nextPos + " but I could not.. retrying later (" + (maxtries - tries) + " tries left)");
				me.doWait(500);
				moved = me.moveTo(nextPos);
				tries++;
			}
			// Interblocking
			if (!moved) {
				// TODO
				me.debug("Something in front of me doesn't want me to go");
				this.finished = true;
				this.returnStatus = CollectorBehaviour.EV_BLOCKED;
			} else {
				this.path.remove(0);
			}
		}
	}

	@Override
	public boolean done() {
		return this.finished;
	}

	@Override
	public int onEnd() {
		this.finished = false;
		return this.returnStatus;
	}
}
