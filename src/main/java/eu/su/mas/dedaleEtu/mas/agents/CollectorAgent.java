package eu.su.mas.dedaleEtu.mas.agents;

import java.util.List;
import java.util.ArrayList;

import jade.core.behaviours.Behaviour;

import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.collect.CollectorBehaviour;

public class CollectorAgent
extends AbstractAgent {
	private static final long serialVersionUID = 2923219269375896321L;

	private String tankPosition;
	// TODO Find out Tank's name by DF
	// This should be the local name of the agent (ie: agent.getLocalName())
	private String tankName = "Tanker1";
	private String goldPosition;
	private Job job;
	private int gold;

	public enum Job {
		LOCKPICK, COLLECT
	};

	@Override
	protected void setup() {
		super.setup();
		this.gold = 0;
		List<Behaviour> lb = new ArrayList<>();
		lb.add(new CollectorBehaviour(this));
		addBehaviour(new startMyBehaviours(this, lb));
	}

	public String getTankPosition() {
		return this.tankPosition;
	}

	public void setTankPosition(String pos) {
		this.tankPosition = pos;
	}

	// This should be the local name of the agent (ie: agent.getLocalName())
	public String getTankName() {
		return this.tankName;
	}

	public String getGoldPosition() {
		return this.goldPosition;
	}

	public int getGold() {
		return this.gold;
	}


	public void addGold(int gold) {
		this.gold += gold;
	}

	public void emptyGold() {
		this.gold = 0;
	}


	public void setGoldPosition(String pos) {
		this.goldPosition = pos;
	}

	public Job getJob() {
		return this.job;
	}

	public void setJob(Job job) {
		this.job = job;
	}


	@Override
	protected void beforeMove() {
		super.beforeMove();
	}

	@Override
	protected void afterMove() {
		super.afterMove();
	}
}
