package eu.su.mas.dedaleEtu.mas.behaviours.share;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public abstract class CommunicationBehaviour
extends SimpleBehaviour {
	private static final long serialVersionUID = 4470544376077192440L;

	public CommunicationBehaviour(final AbstractAgent agent) {
		super(agent);
	}

	/**
	 * Gets all agents available, except for the one making the request.
	 *
	 * @deprecated Use {@link DFService} instead.
	 * @return a list of agents
	 */
	@Deprecated
	public List<AID> getAllAgents() {
		final List<String> agentNames = ((AbstractAgent) this.myAgent).getAgents();
		return agentNames.stream()
		.filter(n -> !n.equals(this.myAgent.getAID().getLocalName()))
		.map(name -> new AID(name, AID.ISLOCALNAME))
		.collect(Collectors.toList());
	}

	/**
	 * Sends a message to the given receivers.
	 *
	 * @param receivers list of agents
	 * @param content content
	 * @param performative FIPA performative
	 */
	public void sendMessage(final Collection<AID> receivers, final String content, final int performative) {
		final ACLMessage msg = new ACLMessage(performative);
		msg.setSender(this.myAgent.getAID());
		receivers.forEach(receiver -> msg.addReceiver(receiver));
		msg.setContent(content);
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg);
	}

	/**
	 * Sends a message to the given receivers.
	 *
	 * @param receivers list of agents
	 * @param contentObject content object
	 * @param performative FIPA performative
	 * @throws IOException
	 */
	public void sendMessage(final Collection<AID> receivers, final Serializable contentObject, final int performative)
	throws IOException {
		final ACLMessage msg = new ACLMessage(performative);
		msg.setSender(this.myAgent.getAID());
		receivers.forEach(receiver -> msg.addReceiver(receiver));
		msg.setContentObject(contentObject);
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg);
	}

	/**
	 * Receives a new message.
	 *
	 * @param messageTemplate message template to match against.
	 * @param startTime initial waiting time
	 * @param timeout timeout
	 * @return a message if one was received, null otherwise.
	 */
	public ACLMessage waitMessage(final MessageTemplate messageTemplate, final long startTime, final long timeout) {
		ACLMessage msg = this.myAgent.receive(messageTemplate);
		while (msg == null && System.currentTimeMillis() - startTime < timeout) {
			try {
				this.myAgent.doWait(timeout / 5);
			} catch (final Exception e) {
				System.err.println("Agent " + this.myAgent.getLocalName() + " failed to wait");
				e.printStackTrace();
			}
			msg = this.myAgent.receive(messageTemplate);
		}
		return msg;
	}

	/**
	 * Receives a new message.
	 *
	 * @param messageTemplate message template to match against.
	 * @param timeout timeout
	 * @return a message if one was received, null otherwise.
	 */
	public ACLMessage waitMessage(final MessageTemplate messageTemplate, final long timeout) {
		return this.waitMessage(messageTemplate, System.currentTimeMillis(), timeout);
	}

	/**
	 * Clear all old messages from this sender.
	 *
	 * @param sender sender
	 */
	public void clearOldMessages(final AID sender) {
		ACLMessage oldMsg;
		do {
			oldMsg = this.waitMessage(MessageTemplate.MatchSender(sender), 0);
		} while (oldMsg != null);
	}
}
