package eu.su.mas.dedaleEtu.mas.behaviours.collect;

import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

public class MakeSummaryBehaviour
extends SimpleBehaviour {
	private static final long serialVersionUID = -5466124881716672156L;

	private boolean finished;
	private int returnStatus;

	public MakeSummaryBehaviour(final CollectorAgent agent) {
		super(agent);
		this.finished = false;
	}

	@Override
	public void action() {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		final ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setProtocol("collection");
		msg.setSender(me.getAID());
		msg.addReceiver(new AID(me.getTankName(), true));
		String content = "";
		content += "transfer";
		content += " " + me.getGold();
		msg.setContent(content);

		me.emptyMyBackPack(me.getTankName());
		me.emptyGold();
		me.sendMessage(msg);
		this.finished = true;
		this.returnStatus = CollectorBehaviour.EV_AVAILABLE;
	}

	@Override
	public boolean done() {
		return this.finished;
	}

	@Override
	public int onEnd() {
		this.finished = false;
		return this.returnStatus;
	}
}
