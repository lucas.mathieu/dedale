package eu.su.mas.dedaleEtu.mas.behaviours.share;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.dispatch.TankerBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.explore.ExplorationBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.SerializableGraph;
import jade.core.AID;
import jade.core.behaviours.CompositeBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class ShareMapBehaviour
extends CommunicationBehaviour {
	private static final long serialVersionUID = 4138428140462845638L;

	private boolean finished;
	private int transition;
	private final MapRepresentation map;
	private List<String> sharedAgents;
	private long startTime;
	private long startMapTime;
	private long lastPing;

	private static final int PING_INTERVAL = 20;
	private static final int PING_TIMEOUT = 20;
	private static final int MAP_TIMEOUT = 100;
	private static final int BEHAVIOUR_TIMEOUT = 300;

	private final MessageTemplate pingTemplate = MessageTemplate.MatchContent("ping");
	private final MessageTemplate pongTemplate = MessageTemplate.MatchContent("pong");
	private final MessageTemplate mapTemplate = new MessageTemplate(msg -> {
		try {
			return msg.getContentObject() instanceof SerializableGraph;
		} catch (final UnreadableException e) {
			return false;
		}
	});

	public ShareMapBehaviour(final AbstractAgent agent, final MapRepresentation map) {
		super(agent);
		this.map = map;
	}

	@Override
	public void action() {
		try {
			if (this.sharedAgents == null) {
				this.sharedAgents = new ArrayList<>();
				this.startTime = this.currentTime();
				this.lastPing = 0;
			}
			final ACLMessage msg = this.checkMessages();

			if (this.currentTime() - this.startTime > BEHAVIOUR_TIMEOUT) {
				this.quit();
			}
			else if (msg == null) {
				this.sendPing();
				final ACLMessage msg2 = this.waitMessage(MessageTemplate.or(
					this.pongTemplate,
					this.mapTemplate
				), this.startTime, PING_TIMEOUT);

				if (msg2 != null) {
					if (this.pongTemplate.match(msg2)) {
						this.sendMap(msg2.getSender());
						this.retrieveMap();
					} else if (this.mapTemplate.match(msg2)) {
						this.mergeMap(msg2);
						this.excludeAgent(msg2.getSender());
					} else {
						// Do nothing
					}
				} else {
					// Timeout : try other ping by looping
				}
			} else if (this.pingTemplate.match(msg)) {
				this.sendPong(msg.getSender());
				this.sendMap(msg.getSender());
				this.retrieveMap();
			} else if (this.mapTemplate.match(msg)) {
				this.mergeMap(msg);
			} else {
				// Pong: ignore it (either it's too late or we already sent the map)
			}
		}
		catch (final Exception e) {
			e.printStackTrace();
			this.quit();
		}
	}

	private long currentTime() {
		return System.currentTimeMillis();
	}

	private void quit() {
		final CompositeBehaviour cb = this.getParent();
		if (cb instanceof ExplorationBehaviour) {
			final ExplorationBehaviour eb = (ExplorationBehaviour) cb;
			final String st = eb.getLastState();
			if (st.equals(ExplorationBehaviour.STATE_EXPLORE)) {
				this.transition = ExplorationBehaviour.TRANSITION_SHARE_EXPLORE;
			} else if (st.equals(ExplorationBehaviour.STATE_BLOCK)) {
				this.transition = ExplorationBehaviour.TRANSITION_SHARE_BLOCK;
			} else {
				this.transition = -1; // FIXME
			}
		}
		else if (cb instanceof TankerBehaviour) {
			this.transition = TankerBehaviour.TRANSITION_SHARE_DISPATCH;
		}
		this.finished = true;
	}

	private void excludeAgent(final AID sender) {
		this.sharedAgents.add(sender.getLocalName());
	}

	private ACLMessage checkMessages() {
		final MessageTemplate mt = MessageTemplate.or(
			this.mapTemplate,
			MessageTemplate.or(
				this.pingTemplate,
				this.pongTemplate
			)
		);
		return this.myAgent.receive(mt);
	}

	private void sendPing() {
		if (this.currentTime() - this.lastPing < PING_INTERVAL) {
			return;
		}
		final List<AID> receivers = this.getAllAgents()
		.stream()
		.filter(agent -> !this.sharedAgents.contains(agent.getLocalName()))
		.collect(Collectors.toList());
		this.sendMessage(receivers, "ping", ACLMessage.REQUEST);
		this.lastPing = this.currentTime();
	}

	private void sendPong(final AID receiver) {
		System.out.println(this.myAgent.getLocalName() + ": Sending Pong");
		this.sendMessage(Arrays.asList(receiver), "pong", ACLMessage.INFORM);
	}

	private void sendMap(final AID receiver)
	throws IOException {
		System.out.println(this.myAgent.getLocalName() + ": Sending Map");
		this.sendMessage(Arrays.asList(receiver), this.map.getSerializableGraph(), ACLMessage.INFORM);
	}

	private void mergeMap(final ACLMessage msg)
	throws UnreadableException {
		System.out.println(this.myAgent.getLocalName() + ": Merging");
		final SerializableGraph sg = (SerializableGraph) msg.getContentObject();
		this.map.mergeGraph(sg);
		clearOldMessages(msg.getSender());
		this.quit();
	}

	private void retrieveMap()
	throws UnreadableException {
		this.startMapTime = this.currentTime();
		final ACLMessage msg2 = this.waitMessage(this.mapTemplate, this.startMapTime, MAP_TIMEOUT);
		if (msg2 != null) {
			this.mergeMap(msg2);
			this.excludeAgent(msg2.getSender());
		}
	}

	@Override
	public int onEnd() {
		// TODO
		this.sharedAgents = null;
		return this.transition;
	}

	@Override
	public boolean done() {
		if (this.finished) {
			this.finished = false;
			return true;
		}
		return false;
	}
}
