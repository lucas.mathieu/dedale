package eu.su.mas.dedaleEtu.mas.behaviours.collect;

import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import jade.core.behaviours.SimpleBehaviour;

public class CollectGoldBehaviour
extends SimpleBehaviour {
	private static final long serialVersionUID = 4172314626407952526L;
	
	private boolean finished;
	private int returnStatus;

	public CollectGoldBehaviour(final CollectorAgent agent) {
		super(agent);
		this.finished = false;
	}

	@Override
	public void action() {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		if (me.getJob() == CollectorAgent.Job.COLLECT) {
			// Check chest status
			if (this.isChestOpened()) {
				final Integer available = me.observe()
					.stream()
					.filter(c -> c.getLeft().equals(me.getCurrentPosition()))
					.findAny()
					.get()
					.getRight()
					.stream()
					.filter(c -> c.getLeft() == Observation.GOLD)
					.findAny()
					.get()
					.getRight();
				if (available == null) {
					// No chest ?!
				} else if (available > 0) {
					final Integer gold = me.pick();
					if (gold == 0) {
						// Can be normal
						if (me.getBackPackFreeSpace() == 0) {
							me.println("I tried to pick gold, but my backpack is full :/");
						} else {
							me.println("It seems like I'm not authorize to pick gold here, even though my backback has space");
						}
					} else {
						me.println("I picked up " + gold + " gold!");
						me.addGold(gold);
						this.finished = true;
						this.returnStatus = CollectorBehaviour.EV_COLLECTED;
					}
				} else {
					this.finished = true;
					this.returnStatus = CollectorBehaviour.EV_COLLECTED;
				}
			}
			// Chest is closed
			else {
				// TODO
				final boolean opened = me.openLock(Observation.GOLD);
				// check around
				// requirements
				// open
				if (opened) {
					me.println("I successfully opened the lock!");
					// TODO
					// people go home! or pick after me!
				} else {
					me.println("I couldn't open the lock!");
				}
			}
		} else if (me.getJob() == CollectorAgent.Job.LOCKPICK) {
			// TODO
			// what is happening ?
			// did we open the chest ?
			// are we waiting to open the chest ?
			// are there even people here ?
			// should I pick the chest ?
			// wait for a message...
			me.println("I am supposed to lock pick, and I am currently waiting");
			me.doWait(1000);
		} else {
			// Uknown job
			me.println("I am unemployed, I don't have a job");
			me.doWait(1000);
		}
	}

	private boolean isChestOpened() {
		final CollectorAgent me = (CollectorAgent) this.myAgent;
		try {
			return me.observe()
				.stream()
				.filter(c -> c.getLeft().equals(me.getCurrentPosition()))
				.findAny()
				.get()
				.getRight()
				.stream()
				.filter(c -> c.getLeft() == Observation.LOCKSTATUS)
				.findAny()
				.get()
				.getRight() == 0;
		} catch (final Exception e) {
			return false;
		}
	}

	@Override
	public boolean done() {
		return this.finished;
	}

	@Override
	public int onEnd() {
		this.finished = false;
		return this.returnStatus;
	}
}
