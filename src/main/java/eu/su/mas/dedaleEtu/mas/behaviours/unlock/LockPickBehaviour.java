package eu.su.mas.dedaleEtu.mas.behaviours.unlock;

import eu.su.mas.dedaleEtu.mas.agents.AbstractAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.SimpleBehaviour;

public class LockPickBehaviour
extends SimpleBehaviour {
	private static final long serialVersionUID = 2746844486132057185L;

	private boolean finished;
	private final MapRepresentation map;

	public LockPickBehaviour(final AbstractAgent agent, final MapRepresentation map) {
		this.map = map;
	}

	@Override
	public void action() {

	}

	@Override
	public boolean done() {
		return this.finished;
	}

}
