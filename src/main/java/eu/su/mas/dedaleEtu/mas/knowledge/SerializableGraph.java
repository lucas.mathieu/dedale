package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import dataStructures.tuple.Couple;

public class SerializableGraph implements Serializable {
	private static final long serialVersionUID = -3662744855411610644L;

	private final String id;
	private final HashSet<SerializableNode> nodes;
	private final List<Couple<String, String>> edges;

	public SerializableGraph(final Graph graph) {
		this.id = graph.getId();
		this.nodes = new HashSet<>();
		this.edges = new ArrayList<>();
		for (final Node node : graph.getEachNode()) {
			this.nodes.add(new SerializableNode(node));
		}
		for (final Edge edge : graph.getEachEdge()) {
			this.edges.add(new Couple<>(edge.getNode0().getId(), edge.getNode1().getId()));
		}
	}

	public Graph toGraph() {
		final Graph graph = new SingleGraph(this.id);
		this.nodes.forEach(node -> {
			Node n = graph.addNode(node.getId());
			node.getAttributes().forEach((key, val) -> n.addAttribute(key, val));

		});
		this.edges.forEach(c -> graph.addEdge(Integer.toString(graph.getEdgeCount() + 1), c.getLeft(), c.getRight()));
		return graph;
	}

	class SerializableNode implements Serializable {
		private static final long serialVersionUID = -3749671114985974257L;

		private final String id;
		private final HashMap<String, Serializable> attributes;

		public SerializableNode(final Node node) {
			this.id = node.getId();
			this.attributes = new HashMap<>();
			node.getAttributeKeyIterator().forEachRemaining(
				key -> {
					Object obj = node.getAttribute(key);
					if (obj instanceof Serializable) {
						Serializable s = (Serializable) obj;
						attributes.put(key, s);
					}
				}
			);
		}

		public SerializableNode(final String id) {
			this.id = id;
			this.attributes = new HashMap<>();
		}

		public String getId() {
			return SerializableNode.this.id;
		}

		public HashMap<String, Serializable> getAttributes () {
			return this.attributes;
		}


		@Override
		public boolean equals(final Object obj) {
			return (obj instanceof SerializableNode) ? ((SerializableNode) obj).id == SerializableNode.this.id : false;
		}
	}

	public HashSet<SerializableNode> getNodes() {
		return this.nodes;
	}

	public List<Couple<String, String>> getEdges() {
		return this.edges;
	}
}
